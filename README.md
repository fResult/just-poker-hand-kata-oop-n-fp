# Poker Hand Kata - OOP vs FP in TypeScript

This kata is to demonstrate different ways of acheiving the same results using different pardigmns, (OOP vs FP). 

One paradigmn is not superior than the other as long as developer followed good design principles such as SOLID.

# More about the kata

http://codingdojo.org/kata/PokerHands/

## Adjust
* defined explicit types
* used enums for duplicated values
* adjusted logic on some functions
